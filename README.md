# Consulta boletas electrónicas #

Proyecto integrado por:

API REST = para hacer consulta si es socio la persona que consulte la boleta.

WebService SOAP: consumo de boletas de los clientes, a través de un webservice externo.

Es un proyecto realizado con PHP llamando con los servicios API REST mediante CURL y para el webservices mediante SoapClient. 

Además integrado con un framework de diseño Bootstrap 3.3.6.