<?php
/**********************************************************
**********************************************************
FRANK:
VARIABLES PARA USAR EN WEBSERVICES
**********************************************************
**********************************************************/
$Receiver = '16047644-3';
$Num = rand(0,9);
$Num2 = rand(0,9);
$Num3 = rand(0,9);
$Num4 = rand(0,9);
$NumTotal = $Num + $Num2;
$NumTotal2 = $Num3 + $Num4;

/**********************************************************
**********************************************************
FRANK:
VERIFICA SI ES SOCIO VIGENTE O NO MEDIANTE REST GET
UTILIZANDO CURL (LLAMAR MEDIANTE CURL PHP)
**********************************************************
**********************************************************/
//FELIPE 16047644-3
//FRANK 15640947-2
$access_key = 'Pr0D_XeFnMUadhGNM3VA6SGYBArxZMQYuDXXuD26s#Pr0D_Dak7UmQJy5WXWYNExEMqMEPry6GUckNY7jY';
$curl = curl_init( 'http://190.98.209.5:90/ws/web/socios_activo/'.$Receiver);
curl_setopt( $curl, CURLOPT_HTTPHEADER, array( 'Authorization:' . $access_key,'Content-type: application/json') );
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$resultado = curl_exec($curl);
curl_close($curl);
$resultado = json_decode(stripslashes($resultado), true);


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Consultas boletas CAV</title>

    <!-- Bootstrap core CSS -->
    <link href=" css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href=" css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<link href=" css/dte_css.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/starter-template.css" rel="stylesheet">
    <link href="css/jquery.fancybox.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src=" js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style rel="stylesheet">
	/*Progress Bar*/
		#upload-progress{
			height: 20px;
			border: 1px solid #ddd;
			width: 100%;
		}
		#upload-progress .progress-bar{
			background: #bde1ff;
			width: 0;
			height: 20px;
		}


	</style>



  </head>

  <body>

    <?php include('nav_dte.php'); ?>

    <div class="container masterdte">
			<div class="row">
				<h2 class="dtept">CONSULTA BOLETA ELECTR&Oacute;NICA</h2>
				<br>
				<div id="server-results"></div>

				<div class="col-xs-12 col-lg-6 panelconsultas">

					<div class="panel panel-danger panelsocios">
						<div class="panel-heading">
					    <h2>Consulta boletas socios</h2>
					  </div>
					  <div class="panel-body">
							<form class="form-horizontal" id="form_consult">
								<div class="form-group">
									<label class="col-sm-2 control-label">RUT</label>
							    <div class="col-sm-5">
							      <input type="text" class="form-control" id="rut" name="rut" placeholder="ingrese su rut" value="">
							    </div>
								</div>
								<div class="form-group">
							    <div class="col-sm-offset-2 col-sm-10">
										<button type="submit" name="submit" class="btn btn-danger" id="btnsubmit" disabled>Entrar</button>
							    </div>
							  </div>

							</form>
					  </div>
					</div>

				</div>
				<div class="col-xs-12 col-lg-6 panelconsultas">

					<div class="panel panel-danger panelsocios">
						<div class="panel-heading">
					    <h2>Consulta clientes</h2>
					  </div>
					  <div class="panel-body">
							<form class="form-horizontal valido2" id="form_consult_nosocio"  >
								<div class="form-group">
									<label class="col-sm-4 control-label">TIPO DE DOCUMENTO</label>
							    <div class="col-sm-5">
							      <select class="form-control" name="DocumentType" id="DocumentType">
							      	<option value="">Seleccione un documento</option>
											<option value="39">Boletas</option>
							      </select>
							    </div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">N&deg; DE FOLIO</label>
							    <div class="col-sm-5">
							      <input type="text" class="form-control" id="DocumentNumber" name="DocumentNumber" placeholder="Ingrese n&uacute;mero de folio" value="">
							    </div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">MONTO TOTAL</label>
							    <div class="col-sm-5">
							      <input type="text" class="form-control" id="monto_total" name="monto_total" placeholder="ingrese monto total" value="">
							    </div>
								</div>
                <!--CAPTCHA-->
                <div class="form-group dte_inp">
                 <label class="col-sm-4" for="Captchat">Escriba el resultado de la suma: <?php echo $Num3;?>&nbsp;&nbsp;+&nbsp;&nbsp; <?php echo $Num4;?>&nbsp;&nbsp;=&nbsp;&nbsp;</label>
                <!--captcha-->
                 <br/>
                 <div class="col-sm-5">
                   <input type="text" name="calc2" id="calc" class="campo captcha form-control" size="2" maxlength="4"/>
                 </div>
                 <!--fin captcha-->
                <input type="hidden" name="numbers" id="numbers" value="<?php echo $NumTotal2;?>" />
                 </div><!--fin form-group-->
                <!--CAPTCHA-->
								<div class="form-group">
							    <div class="col-sm-offset-4 col-sm-10">
										<button type="submit" class="btn btn-danger" value="Consultar">Consultar</button>
							    </div>
							  </div>

							</form>
              <div class="err" id="add_err"></div>
					  </div>
					</div>

				</div>
			</div>
    </div><!-- /.container -->

<?php
//$codcav = 'a1';

if(isset($_GET['codcav']) == "a1"){
?>
<script type="text/javascript">

</script>
<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h3>Estimado/a,</h3><br>
        <p>En nuestro sistema no figura como socio de la CAV</p>
      </div>
      <div class="modal-footer">
        <a href="/m_dte" class="btn btn-primary btnvol">Volver</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
}
?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src=" js/vendor/jquery.min.js"><\/script>')</script>
    <script src=" js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=" js/ie10-viewport-bug-workaround.js"></script>
		<script src="js/jquery.rut.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
		<script type="text/javascript">
		$("input#rut").rut({formatOn: 'keyup',
												ignoreControlKeys: false,
												validateOn: 'change' // si no se quiere validar, pasar null
											}).on('rutInvalido', function(e) {
														$('#btnsubmit').attr('disabled',true);

													}).on('rutValido', function(e, rut, dv) {
														$('#btnsubmit').attr('disabled',false);
													});

$(document).ready(function() {

		$("#form_consult").on("submit",function(event){

				event.preventDefault(); //prevent default action
				var form_data = new FormData(this); //Encode form elements for submission

				$.ajax({
						url : 'boletas_socio.php',
						type: 'POST',
						data : form_data,
            contentType: false,
            processData:false,
            cache: false,
            async: false,
				}).done(function(data){
          $('#server-results').fadeOut('slow', function(){
               $('.panelconsultas').hide().html(data);
               $('#server-results').fadeIn('slow').html(data);
            });
        });

		});
    //NO SOCIO

    $('#form_consult_nosocio').validate({
           rules: {
            DocumentType: "required",
            DocumentNumber: {
                required: true,
                number: true,
                digits: true
            },
            monto_total: {
                required: true,
                number: true,
                digits: true
            },
               calc2: { equalTo: '#numbers', required: true }
           },
           messages:{
               DocumentNumber: "&nbsp;&nbsp;&nbsp;Debes ingresar el n&uacutemero de Folio.",
               DocumentType: "&nbsp;&nbsp;&nbsp;Debes seleccionar un documento.",
               monto_total: "&nbsp;&nbsp;&nbsp;Debes ingresar el monto Total.",
               calc2 : '&nbsp;&nbsp;El n&uacute;mero no es la suma exacta'
           },
           submitHandler:function(form){
             //DECLARAR VARIABLES
            /* var DocumentType = $('#DocumentType').val();
             var DocumentNumber = $('#DocumentNumber').val();
             var monto_total = $('#monto_total').val();*/

                 //event.preventDefault(e); //prevent default action
                 //var form_data2 = new FormData(this); //Encode form elements for submission
                 //var form_data = $( "form#form_consult_nosocio" ).serialize();
                 form_data = new FormData($("form#form_consult_nosocio")[0]);
                // alert(form_data);
                 $.ajax({
                     url : 'boletas_nosocio.php',
                     type: 'POST',
                     data: form_data,
                     contentType: false,
                     processData:false,
                     cache: false,
                     async: false,
                    success:function(response){
                            $('#server-results').fadeOut('slow', function(){
                           $('.panelconsultas').hide();
                           $('#server-results').fadeIn('slow').html(response);
                        });
                              }
                 });


           }
      });
    /*$('form2').validate({
                           rules: {
                               id: "required"
                           },
                           messages:{
                               id: '<center>Debes seleccionar una opci&oacute;n</center>'
                           }
                       });*/



$('.modal').modal('show')
});

		</script>
  </body>
</html>
