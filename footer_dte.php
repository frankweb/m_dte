<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src=" js/vendor/jquery.min.js"><\/script>')</script>
<script src=" js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src=" js/ie10-viewport-bug-workaround.js"></script>
<script src="js/jquery.rut.js"></script>
<script type="text/javascript">
$("input#rut").rut({formatOn: 'keyup',
                    ignoreControlKeys: false,
                    validateOn: 'change' // si no se quiere validar, pasar null
                  }).on('rutInvalido', function(e) {
                        $('#btnsubmit').attr('disabled',true);

                      }).on('rutValido', function(e, rut, dv) {
                        $('#btnsubmit').attr('disabled',false);
                      });


$("#form_consult").submit(function(event){

    event.preventDefault(); //prevent default action
    var form_data = new FormData(this); //Encode form elements for submission

    $.ajax({
        url : 'boletas_nosocio.php',
        type: 'POST',
        data : form_data,
        contentType: false,
        processData:false,
        cache: false,
        dataType: 'html',
        xhr: function(){
    //upload Progress
    var xhr = $.ajaxSettings.xhr();
    if (xhr.upload) {
        xhr.upload.addEventListener('progress', function(event) {
            var percent = 0;
            var position = event.loaded || event.position;
            var total = event.total;
            if (event.lengthComputable) {
                percent = Math.ceil(position / total * 100);
            }
            //update progressbar
            $("#upload-progress .progress-bar").css("width", + percent +"%");

        }, true);
    }
    return xhr;
}
    }).done(function(data){ //
      $(".panelconsultas").hide('slow');
    $("#server-results").show('slow').html(data);
});
});
//NO SOCIO
$("#form_consult_nosocio").submit(function(event){

    event.preventDefault(); //prevent default action
    var form_data = new FormData(this); //Encode form elements for submission

    $.ajax({
        url : 'boletas_nosocio.php',
        type: 'POST',
        data : form_data,
        contentType: false,
        processData:false,
        cache: false,
        dataType: 'html',
        xhr: function(){
    //upload Progress
    var xhr = $.ajaxSettings.xhr();
    if (xhr.upload) {
        xhr.upload.addEventListener('progress', function(event) {
            var percent = 0;
            var position = event.loaded || event.position;
            var total = event.total;
            if (event.lengthComputable) {
                percent = Math.ceil(position / total * 100);
            }
            //update progressbar
            $("#upload-progress .progress-bar").css("width", + percent +"%");

        }, true);
    }
    return xhr;
}
    }).done(function(data){ //
      $(".panelconsultas").hide('slow');
    $("#server-results").show('slow').html(data);
});
});
</script>
